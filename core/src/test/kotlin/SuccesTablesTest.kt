package de.bstachmann.rpgdiceutils

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.apache.commons.math3.fraction.Fraction

class SuccesTablesTest : StringSpec({

    "success tables".config(enabled = true) {
        val dramaticD12 = Roll.D(12).makeDramatic()
        (1..12)
            .map { p -> successTable(Fraction(p, 12)).mapTo(dramaticD12) }
            .forEach { t ->
                t.outcomes.fold(Fraction.ZERO) { acc, o -> acc.add(o.p) } shouldBe Fraction.ONE
            }
    }


    "success tables - symmetry".config(enabled = true) {
        listOf(
            Roll.D(6).makeDramatic(),
            Roll.D(2),
            Roll.D(100),
            Roll.D(12).makeDramatic()
        ).forEach { die ->
            (1..143)
                .map { Fraction(it, 144) }
                .map { p -> p to successTable(p).mapTo(die) }
                .forEach { (probability, t) ->
                    val inverseTable = successTable(
                        Fraction.ONE.subtract(probability)
                    ).mapTo(die)
                    println("Hu!")
                    println(t.description())
                    println(inverseTable.description())

                    t.outcomes.zip(inverseTable.outcomes.reversed())
                        .forEach { (outcome, inverse) -> outcome.p shouldBe inverse.p }
                }
        }
    }

})