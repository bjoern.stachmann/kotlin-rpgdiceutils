package de.bstachmann.rpgdiceutils

import io.kotlintest.specs.StringSpec
import org.apache.commons.math3.fraction.Fraction

class MySuccesTablesTest : StringSpec({

    "success tables - example".config(enabled = false) {
        val d12 = Roll.D(12).makeDramatic()
        listOf(
            1,   // !!! eigentlich Unmöglich   routine
            12,  // !!! knallhart              easy peasy
            24,  // !! knifflig               kann schief gehen
            48,  // ! ob das was wird        wird shon klappen
            72   // Fifty
        )
            .map { Fraction(it, 144) }
            .map { p -> successTable(p).mapTo(d12) }
            .forEach { t -> println(t.description()) }
    }

    "success tables - evaluation".config(enabled = true) {
        val d12 = Roll.D(12).makeDramatic()
        (1..12)
            .map { Fraction(it, 144) }
            .map { p -> successTable(p).mapTo(d12) }
            .forEach { t ->
                val m = t.description()
                //if(m.contains("OK"))
                println(m)
            }
    }
})