import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "de.bstachmann"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.3.72"
}

allprojects {


    repositories {
        jcenter()
        maven("https://kotlin.bintray.com/ktor")
        maven("http://dl.bintray.com/jetbrains/markdown")
    }

    apply(plugin = "org.jetbrains.kotlin.jvm")


    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    dependencies {
        api(kotlin("stdlib-jdk8"))
        api("org.apache.commons:commons-math3:3.6.1")

        implementation("org.jetbrains:markdown:0.1.33")


        testImplementation("io.kotlintest:kotlintest-runner-junit5:3.1.10")
        testImplementation(kotlin("reflect"))

    }

}


task("stage") {
    dependsOn("build")

    doLast {
        println("okidoki")
    }
}
