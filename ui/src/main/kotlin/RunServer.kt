import de.bstachmann.rpgdiceutils.*
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.*
import org.apache.commons.math3.fraction.Fraction
import org.apache.commons.math3.fraction.Fraction.*
import java.lang.Math.*
import kotlin.math.roundToInt

val COLOR_CARDS = "black"
val COLOR_TEXT = "white"
val COLOR_DICE = "white"
val COLOR_DICE_TEXT = "black"

val colors = listOf("#11FF00", "#22DD11", "#33AA22", "#AA3322", "#DD2211", "#FF1100")

fun main() {
    embeddedServer(Netty, 8080) {
        routing {
            get("/") {
                call.respondHtml {
                    body {

                        div {
                            attributes["style"] = "font-size: 0"

                            svg {
                                attributes["width"] = "0"
                                attributes["height"] = "0"

                                declareVieleck("pentagon", 5)
                                declareVieleck("triangle", 3)

                                colors
                                    .forEachIndexed { i, col -> declareGradient("gradient$i", col) }


                            }


                            // 63.5x88 mm
                            // 240 x 333
                            val d12 = Roll.D(12).makeDramatic()
                            listOf(
                                3 to "Eigentlich\nunmöglich",
                                12 to "Kaum zu schaffen",
                                24 to "Sehr schwierig",
                                48 to "Schwierig",
                                72 to "Fifty-Fifty",
                                144 - 48 to "Knifflig",
                                144 - 24 to "Wird schon klappen",
                                144 - 12 to "Fast sicher",
                                144 - 3 to "Routine"
                            )
                                .map { (n, label) -> Fraction(n, 144) to label }
                                .map { (p, label) -> successTable(p, label).mapTo(d12) }
                                .chunked(100)
                                .forEach {
                                    it.forEach { drawTableCard(240, 333, it) }
                                    br()
                                }
                        }
                    }
                }
            }
        }
    }.start(true)
}

private fun SVG.declareVieleck(name: String, n: Int) {
    unsafe {
        val pts = (1..n)
            .map { i ->
                val b = 2 * PI * i / n + PI
                val x = sin(b)
                val y = cos(b)
                "L$x,$y"
            }
            .joinToString(" ")
        raw(
            """
                <def>
                   <path id="$name" d="M0,-1 $pts Z"/>
                </def>
            
            """.trimIndent()
        )

    }
}

private fun SVG.declareGradient(name: String, col: String) {
    unsafe {
        raw(
            """
                <defs>
                    <linearGradient id="$name" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" style="stop-color:$col;stop-opacity:1" />
                        <stop offset="60%" style="stop-color:$col;stop-opacity:0.7" />
                        <stop offset="80%" style="stop-color:$col;stop-opacity:0.3" />
                        <stop offset="100%" style="stop-color:$col;stop-opacity:0" />
                    </linearGradient>
                </defs>
                
            """.trimIndent()
        )
    }
}



private fun HtmlBlockTag.drawTableCard(
    width: Int,
    height: Int,
    table: Roll<MappedOutcome<SuccessFailureOutcome, DramaRollOutcome<UniformFairDieOutcome>>>
) {
    svg {
        attributes["width"] = "$width"
        attributes["height"] = "$height"

        unsafe {


            val u = width / 12
            +"""<rect x="0" y="0" width="$width" height="$height" rx="${u / 2}" ry="${u / 2}" fill="$COLOR_CARDS" stroke="#333333"/>"""
            val border = u / 2
            val titleHeight = 3 * u / 2
            +"""<text x="${width / 2}" y="${border + 3 * u / 4}" font-family="Arial" text-anchor="middle" font-size="$u" font-weight="bold" fill="$COLOR_TEXT" stroke="$COLOR_TEXT">${table.name}</text>"""

            val percent = table
                .outcomes.subList(3, 6)
                .flatMap { it.targetsOutcomes }
                .fold(ZERO) { acc, o -> acc.add(o.p) }
                .multiply(100)
                .toDouble()
                .roundToInt()


            +"""<g transform="translate($border,${border + titleHeight})">"""
            drawContent(width - border * 2, height - border * 2 - titleHeight, u, table)
            +"""</g>"""

            +"""<text x="${width / 2}" y="${height / 2}" font-family="Arial" text-anchor="middle" font-size="${u}" fill="$COLOR_TEXT" stroke="$COLOR_TEXT">$percent%</text>"""
        }
    }
}

private fun Unsafe.drawContent(
    width: Int,
    height: Int,
    u: Int,
    table: Roll<MappedOutcome<SuccessFailureOutcome, DramaRollOutcome<UniformFairDieOutcome>>>
) {

    var p = ZERO
    var n = 1

    val outcomeMargin = 0
    table.outcomes.asReversed().forEach { outcome ->
        val pnext = p.add(outcome.p)
        drawSection(
            p.multiply(height).toInt(),
            height * (n - 1) / 6,
            pnext.multiply(height).toInt(),
            height * n / 6,
            width - outcomeMargin,
            "url(#gradient${n-1})"
        )
        p = pnext
        n += 1
    }

    n = 1
    table.outcomes.asReversed().forEach { outcome ->
        val pnext = p.add(outcome.p)
        val y = height * (n - 1) / 6
        val yNext = height * n / 6
        val ym = (y + yNext) / 2
        val fontSize = u * 6 / 10
        val x = width - u / 2 - outcomeMargin
        drawRequiredDice(outcome.targetsOutcomes.firstOrNull(), x, fontSize, ym - 2)
        drawOutcomeSymbol(outcome.sourceOutcome, colors[n - 1 % colors.size],width,fontSize/2,ym - 2)
        p = pnext
        n += 1
    }
}

private fun Unsafe.drawSection(
    y1l: Int,
    y1r: Int,
    y2l: Int,
    y2r: Int,
    width: Int,
    color: String
) {
    val marginL = 15 * width / 100
    val ml = 5 * width / 10
    val mr = 8 * width / 10
    val marginR = 85 * width / 100
    raw(
        """<path d="
        |M0,$y1l
        |L0,$y2l
        |L$marginL,$y2l
        |C$mr,$y2l $ml,$y2r $marginR,$y2r
        |L$width,$y2r
        |L$width,$y1r
        |L$marginR,$y1r
        |C$ml,$y1r $mr,$y1l $marginL,$y1l
        |L0,$y1l Z" fill="$color" stroke="black" stroke-width="2"/>""".trimMargin()
    )
}

private fun Unsafe.drawRequiredDice(
    minRequiredOutcome: DramaRollOutcome<UniformFairDieOutcome>?,
    width: Int,
    fontSize: Int,
    ty: Int
) {
    var x = width - fontSize / 2

    val secondOutcome = minRequiredOutcome?.secondOutcome

    if (minRequiredOutcome?.firstOutcome?.face == 1 && secondOutcome?.face == 1)
        return;

    if (secondOutcome != null && secondOutcome.face > 1) {
        drawDie(x - fontSize / 4, fontSize, ty + fontSize / 3, secondOutcome, "gray")
        x -= fontSize * 3 / 2
    }

    val firstOutcome = minRequiredOutcome?.firstOutcome
    if (firstOutcome != null) {
        drawDie(x - fontSize / 4, fontSize, ty + fontSize * 2 / 3, firstOutcome, COLOR_DICE)
    }
}

private fun Unsafe.drawDie(
    x: Int,
    fontSize: Int,
    ty: Int,
    firstOutcome: UniformFairDieOutcome,
    color: String
) {
    raw(
        """
            |<use transform="translate(${x - fontSize / 2},${ty - fontSize / 2}) scale(${fontSize})" fill="$color" stroke="black" stroke-width="0.1" xlink:href="#pentagon""/>
            |""".trimMargin()
    )

    val t = firstOutcome.shortDescription
    val tx = if (t.length == 2) x else x - 1 * fontSize / 4
    raw("""<text x="$tx" y="$ty" text-anchor="end" font-size="$fontSize" fill="$COLOR_DICE_TEXT" stroke="$COLOR_DICE_TEXT">$t</text>""")
}

private fun Unsafe.drawOutcomeSymbol(
    minRequiredOutcome: SuccessFailureOutcome,
    color: String,
    width: Int,
    fontSize: Int,
    ty: Int
) {
    val rotation = if (minRequiredOutcome.value.toInt() < 4) 180 else 0
    val x = width - fontSize / 4

    val n = minRequiredOutcome.value.subtract(Fraction(7,2)).abs().add(ONE_HALF).toInt()
    val h = n * 5 * fontSize / 3
    val step = h / n
    (1..n).forEach {
        val y = (ty - h / 2) + it * step
        raw(
            """
                |<use transform="translate(${x - fontSize / 2},${y - fontSize / 2}) scale(${fontSize}) rotate($rotation)" fill="$color" xlink:href="#triangle""/>
                |""".trimMargin()
        )
    }
}
