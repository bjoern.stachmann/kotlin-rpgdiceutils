import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.*
import org.apache.commons.math3.fraction.Fraction
import org.apache.commons.math3.fraction.Fraction.*
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import java.lang.Math.*
import kotlin.math.roundToInt


fun main() {
    val a8Style = """
        
        .pagebreak {
            page-break-before:always;
        }
                                
        .a8-cards {
            display: flex;
            flex-wrap: wrap;
        }
        
        .a8-cards > div {
            width: 74mm;
            height: 52mm;
            padding: 3mm 3mm 3mm 3mm;
            border-style: solid;
            text-align: left;
            font-size: 12px;
        }

    """.trimIndent()

    embeddedServer(Netty, 8090) {
        routing {
            get("/") {
                call.respondHtml {
                    head {
                        style {
                            unsafe {
                                +a8Style
                            }
                        }
                    }

                    body {
                        (1..4).forEach {
                            h1("pagebreak") { +"MOIN" }
                            div(classes = "a8-cards") {

                                (1..8).forEach {
                                    div {
                                        val markdown = """
                                        # Die erste Überschrift
                                        
                                        Dann etwas *Text* mit &
                                        
                                        ## Zweite Überschrift und so.
                                    """.trimIndent()

                                        val flavour = CommonMarkFlavourDescriptor()
                                        val parsedTree = MarkdownParser(flavour).buildMarkdownTreeFromString(markdown)
                                        unsafe {
                                            +HtmlGenerator(markdown, parsedTree, flavour).generateHtml()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }.start(true)
}
