import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


group = "de.bstachmann"
version = "0.1-SNAPSHOT"


plugins {
    kotlin("jvm")
}


dependencies {
    api(kotlin("stdlib-jdk8"))
    api("io.kotlintest:kotlintest-runner-junit5:3.1.10")

}
